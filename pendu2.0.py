# -*-coding:Utf-8 -*
import os
import random
from donnees import *
import pickle
from fonctions import *


# demande du pseudo
print("Entrez votre pseudo")
pseudo = input()

# récupération du dico des scores
nouveau_dico = recup_scores()

# si le pseudo n'existe pas déjà on l'ajoute
if pseudo not in nouveau_dico:
	nouveau_dico[pseudo] = 0

# initialise le score, 0 si nouveau
score = nouveau_dico[pseudo]

# affiche le score de départ
print("Score de départ : ", score)


# choix du mot au hasard
mot = random.choice(liste_mots)

# condition pour continuer les essais
continuer = True

tentatives = 8
lettres_trouvees = []

# début du jeu
print("Début du jeu, veuillez entrer une lettre")

while continuer == True:
	test_1_lettre = True
	# vérifie que 1 seule lettre est entrée
	while test_1_lettre == True:
		lettre = input()
		lettre = lettre.lower()
		if len(lettre) >1:
			test_1_lettre = True
			print("une seule lettre à la fois")
		else:
			test_1_lettre = False

	if lettre in lettres_trouvees:
		print("lettre déjà saisie")

	# si lettre est dans le mot, l'ajouter à la liste des trouvées
	elif lettre in mot:
		add_lettres_tr(lettre, lettres_trouvees)
		print("oui")
		mot_trouve = recup_mot_masque(mot, lettres_trouvees)
		print(mot_trouve) # on affiche le mot avec des * pour les lettres non trouvées
	
	# si lettre pas dans le mot, on a une tentative de moins
	else:
		tentatives -= 1
		print("non, reste ", tentatives, "tentatives")
		mot_trouve = recup_mot_masque(mot, lettres_trouvees)
		print(mot_trouve) # on affiche le mot avec des * pour les lettres non trouvées

	# si on a trouvé toutes les lettres, stoppe la partie
	if mot_trouve.lower() == mot:
		print("Bravo !")
		continuer = False

	# ou si on a plus de tentatives, stoppe aussi
	elif tentatives <= 0:
		print("Game over")
		print("Le mot était : ", mot.upper())
		continuer = False

# quand la partie s'est arrêtée on affiche le score 
score += tentatives
print("Score final: ", score)

# ENREGISTREMENT SCORE
nouveau_dico[pseudo] = score

with open("scores", "wb") as toto:
	pickler1 = pickle.Pickler(toto)
	pickler1.dump(nouveau_dico)




os.system("pause")