# -*-coding:Utf-8 -*
import pickle
import random
import donnees


def recup_scores():
	""" fonction qui récupères les scores pré-enregistrés"""
	with open("scores", "rb") as toto:
		pickler2 = pickle.Unpickler(toto)
		ancien_dico = pickler2.load()
	return ancien_dico



def add_lettres_tr(lettre, lettres_trouvees):
	""" fonction qui ajoute les lettres trouvées à une liste"""
	return lettres_trouvees.append(lettre)


def recup_mot_masque(mot, lettres_trouvees):
	"""Cette fonction renvoie un mot masqué tout ou en partie, en fonction :
	- du mot d'origine (type str)
    - des lettres déjà trouvées (type list)

    On renvoie le mot d'origine avec des * remplaçant les lettres que l'on
    n'a pas encore trouvées."""
	mot_masque = ""
	for lettre in mot:
		if lettre in lettres_trouvees:
			mot_masque += lettre
		else:
			mot_masque += "*"
	return mot_masque.upper()

